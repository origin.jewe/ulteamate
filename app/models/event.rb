class Event < ApplicationRecord

    extend FriendlyId
    friendly_id :name, use: :slugged

    enum division: [ :mixed, :women, :open, :men, :other ]
    enum registration: [ :individual,:team,:both ]
    enum ground: [ :grass, :beach, :indoor, :other ]
    enum level: [ :beginner, :intermediate, :elite, :other ]

end
