class PlayerProfile < ApplicationRecord
    extend FriendlyId
    friendly_id :name, use: :slugged

    has_and_belongs_to_many :teams

    belongs_to :user
end
