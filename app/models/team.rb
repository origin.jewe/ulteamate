class Team < ApplicationRecord
    extend FriendlyId
    friendly_id :name, use: :slugged

    has_and_belongs_to_many :player_profiles

    enum division: [ :mixed, :women, :open, :men, :other ]
    
end
