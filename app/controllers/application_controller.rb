class ApplicationController < ActionController::Base

    def after_sign_in_path_for(resource)
        stored_location_for(resource) || dashboard_path
    end

    def default_url_options
        { host: Rails.application.config.root_host }
    end

end
