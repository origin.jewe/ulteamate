class TeamsController < ApplicationController

    def index
        @teams = Team.all
    end

    def show
        @team = Team.find_by_slug(request.subdomain)
    end

    def new
        @team = Team.new
    end

    def create
        @newteam = Team.new
        @newteam.name = team_params[:name]
        @newteam.location = team_params[:location]
        puts "#######"
        puts team_params[:name]
        puts team_params[:location]
        puts @newteam.inspect
        if @newteam.save!
          redirect_to teams_path
        else
          render "new"
        end
    end

    def team_params
        params.require(:team).permit(:name,:location)
    end
end
