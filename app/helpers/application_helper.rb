module ApplicationHelper

    def team_home(team)
        root_url(subdomain: team.slug)
    end
end
