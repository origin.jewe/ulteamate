Rails.application.routes.draw do
  
  devise_for :users, path: ''

  get '/dashboard', to: "home#dashboard", :as => :dashboard


  resources :teams, :except => [:show]

  get '/play', to: "events#index", :as => :events

  get "/" => "teams#show", :constraints => { :subdomain  => /.+/ }, :as => :team_domain
  root to: "home#index"


end
