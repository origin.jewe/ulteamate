class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.string :name
      t.string :slug
      t.string :location
      t.integer :division
      t.integer :ground
      t.integer :level
      t.date :date
      t.integer :days
      t.string :description
      t.integer :teamfee
      t.integer :playersfee
      t.string :currency

      t.timestamps
    end
  end
end
