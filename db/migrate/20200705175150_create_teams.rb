class CreateTeams < ActiveRecord::Migration[6.0]
  def change
    create_table :teams do |t|
      t.string :name
      t.string :location
      t.integer :division
      t.date :founded

      t.timestamps
    end
  end
end
