class CreatePlayerProfiles < ActiveRecord::Migration[6.0]
  def change
    create_table :player_profiles do |t|
      t.string :name
      t.integer :gender
      t.date :birthday
      t.string :number
      t.integer :position
      t.integer :line
      t.string :email

      t.timestamps
    end
  end
end
