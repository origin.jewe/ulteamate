class AddRegistrationToEvents < ActiveRecord::Migration[6.0]
  def change
    add_column :events, :registration, :integer
  end
end
