class CreateJoinTable < ActiveRecord::Migration[6.0]
  def change
    create_join_table :player_profiles, :teams do |t|
      # t.index [:player_profile_id, :team_id]
      # t.index [:team_id, :player_profile_id]
    end
  end
end
